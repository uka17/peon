module.exports.jobNotFound = 'Job not found';
module.exports.noStepForJob = 'No step found for mentioned jobId';
module.exports.noStepForJobAndStep = 'No step found for mentioned stepId and jobId';
module.exports.itemsUpdated = 'itemsUpdated';
module.exports.itemsDeleted = 'itemsDeleted';
module.exports.message = 'message';